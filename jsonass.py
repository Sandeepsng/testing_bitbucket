import json
import pandas as pd

json_string = '''[{"id":1,"name":"vikash","age":"23","salary":"6000"},{"id":2,"name":"vivek","age":"43","salary":"7000"},
                    {"id":3,"name":"vivek","age":"33","salary":"8000"},{"id":4,"name":"Raj","age":"73","salary":"8000"},
                    {"id":5,"name":"Ram","age":"67","salary":"9800"}]'''

data = json.loads(json_string)

Employee = pd.DataFrame()

for emp in data:
    Employee = Employee.append(emp , ignore_index="True")

Employee = Employee.reindex(columns=['id','name','age','salary'])
Employee.drop_duplicates(subset='name'  , inplace=True)
Employee.set_index('id')

Employee.to_csv('Employee.csv')
